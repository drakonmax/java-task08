import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TestReflectionDemo {
    @Test
    public void testGetNames(){
        class Sample {
            public int getNumber1() {
                return 1;
            }
            private int getNumber2() {
                return 2;
            }
            private String getString() {
                return "a";
            }
            public void someMethod1() {}
            void someMethod2() {}
        }

        Sample sample1 = new Sample();
        Sample sample2 = new Sample();
        Sample sample3 = new Sample();
        Human human1 = new Human("Иванов","Иван","Иванович",20);
        Student student1 = new Student("Комаров","Павел","Андреевич",20, "ИМИТ");
        Human human2 = new Human("Симонов","Иван","Константинович",20);
        Student student2 = new Student("Измайлов","Евгений","Викторович",20, "ИМИТ");
        List<Object> list = new ArrayList<>();
        list.addAll(Arrays.asList(sample1,sample2,sample3,human1,human2,student1,student2));
        int actual = ReflectionDemo.getNames(list);
        assertEquals(4, actual);
    }

    @Test
    public void testMethodsOfObject() {
        class Sample {
            public int getNumber1() {
                return 1;
            }
            private int getNumber2() {
                return 2;
            }
            private String getString() {
                return "a";
            }
            public void someMethod1() {}
            void someMethod2() {}
        }

        Sample obj = new Sample();
        List<String> expected = new ArrayList<>();
        expected.add("equals");
        expected.add("getClass");
        expected.add("getNumber1");
        expected.add("hashCode");
        expected.add("notify");
        expected.add("notifyAll");
        expected.add("someMethod1");
        expected.add("toString");
        expected.add("wait");
        expected.add("wait");
        expected.add("wait");
        List<String> actual = new ArrayList<>();
        actual.addAll(ReflectionDemo.methodsOfObject(obj));
        actual.sort(String::compareTo);
        assertEquals(expected, actual);
    }

    @Test
    public void testNamesOfSuper(){
        Student student = new Student("Иванов","Иван","Иванович",20, "ИМИТ");
        List<String> expected = new ArrayList<>();
        List<String> actual = new ArrayList<>();
        expected.addAll(Arrays.asList("Human","java.lang.Object"));
        actual.addAll(ReflectionDemo.namesOfSuper(student));
        assertEquals(expected, actual);
    }
}
