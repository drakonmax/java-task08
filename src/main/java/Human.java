import java.util.*;

public class Human {
    private String surname;
    private String name;
    private String lastname;
    private int age;

    public Human(String surname, String name, String lastname, int age) {
        this.surname = surname;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }
}
