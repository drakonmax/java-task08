import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Если появится еще один наследник Human, то подсчет не сработает.
     Есть оператор instanceof
    */
    // Москвичёв М.М. 20.06.2020; Исправлено.

    public static int getNames(List<Object> persons) {
        int count = 0;
        for (Object p : persons) {
            if (p instanceof Human)
                count++;
        }
        return count;
    }

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Любой класс содержит как минимум публичный метод toString, а в вашем тесте Sample почему-то не содержит.
    */
    // Москвичёв М.М. 20.06.2020; Исправлено.
    public static List<String> methodsOfObject(Object obj) {
        Class<?> classData = obj.getClass();
        Method[] methods = classData.getMethods();
        ArrayList<String> modNames = new ArrayList<>();
        for (Method method: methods)
        if (Modifier.isPublic(method.getModifiers())){
            modNames.add(method.getName());
        }
        modNames.sort(String::compareTo);
        return modNames;
    }

    public static List<String> namesOfSuper(Object obj){
        Class cls = obj.getClass();
        ArrayList<String> superNames = new ArrayList<>();
        while (cls != Object.class){
            cls = cls.getSuperclass();
            superNames.add(cls.getName());
        }
        return superNames;
    }

}

